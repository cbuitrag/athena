#include "../FastTrackFinderLRTHypoTool.h"
#include "../FastTrackFinderLRTHypoAlg.h"
#include "../TrigIsoHPtTrackTriggerHypoAlg.h"
#include "../TrigIsoHPtTrackTriggerHypoTool.h"
#include "../TrigdEdxTrackTriggerHypoAlg.h"
#include "../TrigdEdxTrackTriggerHypoTool.h"
#include "../TrigHitDVHypoAlg.h"
#include "../TrigHitDVHypoTool.h"
#include "../TrigDisappearingTrackTriggerHypoAlg.h"
#include "../TrigDisappearingTrackTriggerHypoTool.h"

DECLARE_COMPONENT( TrigDisappearingTrackTriggerHypoAlg )
DECLARE_COMPONENT( TrigDisappearingTrackTriggerHypoTool )
DECLARE_COMPONENT( TrigHitDVHypoAlg )
DECLARE_COMPONENT( TrigHitDVHypoTool )
DECLARE_COMPONENT( TrigdEdxTrackTriggerHypoAlg )
DECLARE_COMPONENT( TrigdEdxTrackTriggerHypoTool )
DECLARE_COMPONENT( TrigIsoHPtTrackTriggerHypoAlg )
DECLARE_COMPONENT( TrigIsoHPtTrackTriggerHypoTool )
DECLARE_COMPONENT( FastTrackFinderLRTHypoTool )
DECLARE_COMPONENT( FastTrackFinderLRTHypoAlg )
